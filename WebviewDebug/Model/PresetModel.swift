//
//  PresetModel.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 30/4/2567 BE.
//

import Foundation

public struct PresetModel: Codable {
  
  let protocal: String
  let message: String
  
  init(protocal: String, message: String) {
    self.protocal = protocal
    self.message = message
  }
}
