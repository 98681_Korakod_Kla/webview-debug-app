//
//  ViewController.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit
import WebKit

protocol ViewControllerDelegate {
  func updateJsBridgeList(list: [JSBridgeModel])
  func updateMessageSend(message: MessageModel)
}

class ViewController: UIViewController {
  
  @IBOutlet weak var logTableView: UITableView!
  @IBOutlet weak var webview: WKWebView!
  @IBOutlet weak var urlTextField: UITextField!
  
  // static variable
  let defaultURL = "https://meowing-rowan-pharaoh.glitch.me"
  
  // datamodel
  var jsBridgeList: [JSBridgeModel] = []
  var messageList: [MessageModel] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initialSetup()
  }
  
  func initialSetup() {
    logTableView.delegate = self
    logTableView.dataSource = self
    logTableView.register(UINib(nibName: "LogViewCell", bundle: nil), forCellReuseIdentifier: "logviewcell")
    logTableView.layer.cornerRadius = 5
    logTableView.clipsToBounds = true
    logTableView.isOpaque = false
    logTableView.backgroundColor = UIColor(white: 0, alpha: 0.2)
    webview.layer.cornerRadius = 5
    webview.clipsToBounds = true
    urlTextField.text = defaultURL
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
    
    if #available(iOS 16.4, *) {
      webview.isInspectable = true
    }
  }
  
  func reloadData() {
    UIView.transition(with: logTableView, duration: 0.15, options: .transitionCrossDissolve, animations: {
      self.logTableView.reloadData()
      if self.messageList.count > 0  {
        let indexPath = IndexPath(row: self.messageList.count - 1, section: 0)
        self.logTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
      }
    }, completion: nil)
  }
  
  @IBAction func reloadURLAction(_ sender: Any) {
    let link = URL(string: urlTextField.text ?? defaultURL)!
    let request = URLRequest(url: link)
    webview.load(request)
    updateJsBridgeList(list: DataStore.shared.retrieveData())
  }
  
  @IBAction func sendMessageAction(_ sender: Any) {
    let vc = SendMessageViewController()
    vc.delegate = self
    present(vc, animated: true)
  }
  
  @IBAction func clearLog(_ sender: Any) {
    messageList.removeAll()
    reloadData()
  }
  
  @IBAction func jsBridgeButtonAction(_ sender: Any) {
    let vc = ProtocolViewController()
    vc.delegate = self
    present(vc, animated: true)
  }
  
  @objc func DismissKeyboard(){
  view.endEditing(true)
  }
  
  func showAlert(text: String) {
    let alertController = UIAlertController(title: "Send message Error", message: text, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(okAction)
    present(alertController, animated: true)
  }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return messageList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = logTableView.dequeueReusableCell(withIdentifier: "logviewcell", for: indexPath) as? LogViewCell
    cell?.setupCell(data: messageList[indexPath.row])
    return cell ?? UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let vc = LogViewController()
    vc.data = messageList[indexPath.row]
    present(vc, animated: true)
  }
}

extension ViewController: ViewControllerDelegate {
  func updateJsBridgeList(list: [JSBridgeModel]) {
    guard list.count > 0 else { return }
    webview.configuration.userContentController.removeAllScriptMessageHandlers()
    jsBridgeList = list
    for js in jsBridgeList {
      let value = js.value ?? false
      let name = js.name ?? "-"
      if value {
        webview.configuration.userContentController.add(self, name: name)
      } else {
        webview.configuration.userContentController.removeScriptMessageHandler(forName: name)
      }
    }
  }
  
  func updateMessageSend(message: MessageModel) {
    let model = MessageModel(type: message.type, protocal: message.protocal, message: message.message)
    
    webview.evaluateJavaScript("\(message.protocal)(\"\(message.message)\")", completionHandler: { (_, error) in
      if let err = error {
        self.showAlert(text: "\(err.localizedDescription)" )
      } else {
        self.messageList.append(model)
        self.reloadData()
      }
    })
  }
}

extension ViewController: WKScriptMessageHandler, WKNavigationDelegate {
  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    let result = message.body as? String ?? "message is not a string"
    let channel = message.name
    let model = MessageModel(type: .webview, protocal: channel, message: result)
    messageList.append(model)
    reloadData()
  }
}
