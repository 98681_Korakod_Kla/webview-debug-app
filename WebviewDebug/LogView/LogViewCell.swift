//
//  LogViewCell.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit

public enum CellType {
  case native
  case webview
}

class LogViewCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  @IBOutlet weak var bgView: UIView!
  
  let webviewTitle = "Webview - "
  let nativeTitle = "Native - "
  
  let webviewColor = UIColor(red: 255/255, green: 159/255, blue: 15/255, alpha: 0.41)
  let nativeColor = UIColor(red: 124/255, green: 220/255, blue: 255/255, alpha: 0.41)
  
  override func awakeFromNib() {
    super.awakeFromNib()
    initialSetup()
  }
  
  func initialSetup() {
    bgView.layer.cornerRadius = 5
  }
  
  func setupCell(data: MessageModel) {
    bgView.backgroundColor = data.type == .native ? nativeColor : webviewColor
    titleLabel.text = data.type == .native ? "\(nativeTitle)\(data.protocal)" : "\(webviewTitle)\(data.protocal)"
    messageLabel.text = data.message
    layoutIfNeeded()
  }
  
  override func prepareForReuse() {
    titleLabel.text = ""
    messageLabel.text = ""
  }
}
