//
//  LogViewController.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit

class LogViewController: UIViewController {
  
  @IBOutlet weak var dragBox: UIView!
  @IBOutlet weak var jsonLabel: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  
  @IBOutlet weak var sourceLabel: UILabel!
  @IBOutlet weak var protocolLabel: UILabel!
  
  @IBOutlet weak var mimeType: UILabel!
  
  
  var data: MessageModel?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
  }
  
  func setupView() {
    dragBox.layer.cornerRadius = dragBox.bounds.height/2
    
    sourceLabel.text = data?.type == .native ? "Native" : "Webview"
    protocolLabel.text = data?.protocal ?? "-"
    
    let message = data?.message ?? "-"
    let image = base64ToImage(getBase64ImageFromJson(str: message))
    if image != nil {
      imageView.image = image
    } else {
      let jsonText = prettifyJSON(message)
      if !jsonText.contains("Error while parsing JSON") {
        jsonLabel.text = jsonText
      } else {
        jsonLabel.text = data?.message
      }
    }
  }
  
  // check if there is image inside json
  private func getBase64ImageFromJson(str: String) -> String {
    if str.contains("slipImage") {
      if let subStr = str.split(separator: ":").last {
        return String(subStr).replacingOccurrences(of: "\"", with: "").replacingOccurrences(of: "}", with: "")
      } else {
        return "-"
      }
    } else {
      return "-"
    }
  }
  
  private func prettifyJSON(_ jsonString: String) -> String {
    guard let jsonData = jsonString.data(using: .utf8) else {
      return "Error while parsing JSON"
    }
    do {
      let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as AnyObject
      let prettyJsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
      if let prettyJsonString = String(data: prettyJsonData, encoding: .utf8) {
        return prettyJsonString
      } else {
        return "Error while parsing JSON"
      }
    } catch {
      return "Error while parsing JSON: \(error.localizedDescription)"
    }
  }
  
  private func isBase64Encoded(_ string: String) -> Bool {
      if let data = Data(base64Encoded: string) {
          if let decodedString = String(data: data, encoding: .utf8) {
              return decodedString == string
          }
      }
      return false
  }
  
  func base64ToImage(_ base64String: String) -> UIImage? {
    if let decodedData = Data(base64Encoded: base64String) {
      if decodedData.starts(with: [0xFF, 0xD8]) {
        mimeType.text = ".jpeg"
        return UIImage(data: decodedData)
      } else if decodedData.starts(with: [0x89, 0x50, 0x4E, 0x47]) {
        mimeType.text = ".png"
        return UIImage(data: decodedData)
      } else if decodedData.starts(with: [0x47, 0x49, 0x46]) {
        mimeType.text = ".gif"
        return UIImage(data: decodedData)
      } else {
        mimeType.text = "File is not support"
        return nil
      }
    }
    mimeType.text = "Not an image"
    return nil
  }
}
