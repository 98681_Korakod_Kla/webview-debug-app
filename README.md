# webview-debug-app



## What is it ?

Webview debug application for ios, create for testing webview and native app communication via javascript bridge this application design for testing any webview 

## Feature

- Webview render.
- Webview Message handler by showing via log.
- Dynamic add webview message protocol.
- Enable and disable function for message protocol.
- Save preset for message.
- Send custom message via javascript bridge function.
- persistant store preset data and webview protocol.
- Show error callback for sending message incase webview not prepare javascript function.
- Show Image if webview send image in base64string form.
- show prettier json incase of webview sent json string.


## Screenshot
![image](image/s0.png)
