//
//  SendMessageViewController.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit

class SendMessageViewController: UIViewController {
  
  @IBOutlet weak var backViewController: UIView!
  @IBOutlet weak var dragBox: UIView!
  @IBOutlet weak var messageTextView: UITextView!
  @IBOutlet weak var functionTextField: UITextField!
  @IBOutlet weak var presetTableView: UITableView!
  
  var delegate: ViewControllerDelegate?
  var presetList: [PresetModel]? = [PresetModel(protocal: "finishFaceScan", message: "Sample Message"),
                                    PresetModel(protocal: "finishFatcaCRS", message: "Sample Message"),
                                    PresetModel(protocal: "finishSaveSlip", message: "Sample Message"),
                                    PresetModel(protocal: "keepAlive", message: "Sample Message"),
                                    PresetModel(protocal: "handleGenericError", message: "Sample Message")
  ]
  var cellIdentifier = "presetcell"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    messageTextView.layer.cornerRadius = 5
    messageTextView.layer.masksToBounds = true
    
    presetTableView.delegate = self
    presetTableView.dataSource = self
    presetTableView.register(UINib(nibName: "PresetCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
    tap.cancelsTouchesInView = false
    backViewController.addGestureRecognizer(tap)
    
    initialData()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    DataStore.shared.savePreset(presetList: presetList ?? [])
  }
  
  @objc func DismissKeyboard(){
    view.endEditing(true)
  }
  
  func initialData() {
    let list = DataStore.shared.retrievePreset()
    if !list.isEmpty {
      presetList = DataStore.shared.retrievePreset()
    }
    reloadData()
  }
  
  func reloadData() {
    guard let proList = presetList else { return }
    
    UIView.transition(with: presetTableView, duration: 0.15, options: .transitionCrossDissolve, animations: {
      self.presetTableView.reloadData()
      if proList.count > 0  {
        let indexPath = IndexPath(row: proList.count - 1, section: 0)
        self.presetTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
      }
    }, completion: nil)
  }
  
  @IBAction func sendMessageButtonAction(_ sender: Any) {
    DismissKeyboard()
    guard let functionText = functionTextField.text, !functionText.isEmpty else {
      showAlertEmptyFunction()
      return
    }
    guard let messageText = messageTextView.text else { return }
    var resultMessage: String = ""
    
    // in case of message is json we must replace " to \" for every double quote to let java script accept
    if let data = messageText.data(using: .utf8),
       let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
       let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: [.sortedKeys]) {
      let jsonString = String(data: jsonData, encoding: .utf8)
      resultMessage = jsonString ?? ""
      resultMessage = resultMessage.replacingOccurrences(of: "\"", with: "\\\"")
    } else {
      resultMessage = messageText
    }
    
    let message = MessageModel(type: .native, protocal: functionText, message: resultMessage)
    dismiss(animated: true) {
      self.delegate?.updateMessageSend(message: message)
    }
  }
  
  @IBAction func savePresetButton(_ sender: Any) {
    DismissKeyboard()
    guard let functionText = functionTextField.text, !functionText.isEmpty else {
      showAlertEmptyFunction()
      return
    }
    guard let messageText = messageTextView.text else { return }
    
    let preset  = PresetModel(protocal: functionText, message: messageText)
    presetList?.append(preset)
    reloadData()
  }
  
  func showAlertEmptyFunction() {
    let alertController = UIAlertController(title: "Failed", message: "JS function must not be empty", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Got it", style: .default, handler: nil)
    alertController.addAction(okAction)
    present(alertController, animated: true)
  }
}

extension SendMessageViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return presetList?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = presetTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PresetCell
    cell?.setupCell(data: presetList![indexPath.row])
    
    cell?.deleteAction = {
      self.presetList?.remove(at: indexPath.row)
      self.reloadData()
    }
    
    return cell ?? UITableViewCell()
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    messageTextView.text = presetList?[indexPath.row].message
    functionTextField.text = presetList?[indexPath.row].protocal
  }
  
}
