//
//  DataStore.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import Foundation

class DataStore {
  static let shared = DataStore()
  let persistantStoreKey: String = "jsBridge"
  let presetStoreKey: String = "msPreset"
  
  func saveData(protocolList: [JSBridgeModel]) {
    guard let encoded = try? JSONEncoder().encode(protocolList) else { return }
    UserDefaults.standard.set(encoded, forKey: persistantStoreKey)
  }
  
  func retrieveData() -> [JSBridgeModel] {
    guard let data = UserDefaults.standard.data(forKey: persistantStoreKey) else { return [] }
    guard let decoded = try? JSONDecoder().decode([JSBridgeModel].self, from: data) else { return [] }
    return decoded
  }
  
  func savePreset(presetList: [PresetModel]) {
    guard let encoded = try? JSONEncoder().encode(presetList) else { return }
    UserDefaults.standard.set(encoded, forKey: presetStoreKey)
  }
  
  func retrievePreset() -> [PresetModel] {
    guard let data = UserDefaults.standard.data(forKey: presetStoreKey) else { return [] }
    guard let decoded = try? JSONDecoder().decode([PresetModel].self, from: data) else { return [] }
    return decoded
  }
  
}

