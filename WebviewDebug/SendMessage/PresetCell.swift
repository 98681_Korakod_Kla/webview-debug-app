//
//  PresetCell.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 30/4/2567 BE.
//

import UIKit

class PresetCell: UITableViewCell {
  
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var protocolLabel: UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  
  var deleteAction: (() -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    bgView.layer.cornerRadius = 5
  }
  
  func setupCell(data: PresetModel) {
    protocolLabel.text = data.protocal
    messageLabel.text = data.message
  }
  
  
  @IBAction func deleteButtonAction(_ sender: Any) {
    deleteAction?()
  }
  
  override func prepareForReuse() {
    protocolLabel.text = ""
    messageLabel.text = ""
    deleteAction = nil
  }
}
