//
//  MessageModel.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import Foundation

public struct MessageModel {
  
  let type: CellType
  let protocal: String
  let message: String
  
  init(type: CellType, protocal: String, message: String) {
    self.type = type
    self.protocal = protocal
    self.message = message
  }
}
