//
//  ProtocalViewController.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit

class ProtocolViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var dragBOx: UIView!
  @IBOutlet weak var protocolListTableView: UITableView!
  @IBOutlet weak var protocolTextField: UITextField!
  @IBOutlet weak var effectView: UIView!
  
  let cellIdentifier: String = "protocolcell"
  
  var delegate: ViewControllerDelegate?
  var protocolList: [JSBridgeModel]? = [JSBridgeModel.init(name: "navigateToLifeStyleLanding", value: true),
                                        JSBridgeModel.init(name: "navigateToFaceScan", value: true),
                                        JSBridgeModel.init(name: "navigateToFatcaCRS", value: true),
                                        JSBridgeModel.init(name: "saveSlip", value: true),
                                        JSBridgeModel.init(name: "navigateToAccountSummary", value: true)
  ]

  override func viewDidLoad() {
    super.viewDidLoad()
    setupView()
    
    
    initialdata()
  }
  
  func initialdata() {
    let list = DataStore.shared.retrieveData()
    if !list.isEmpty {
      protocolList = list
    }
    reloadData()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    DataStore.shared.saveData(protocolList: protocolList ?? [])
    delegate?.updateJsBridgeList(list: protocolList ?? [])
  }
  
  func setupView() {
    protocolListTableView.delegate = self
    protocolListTableView.dataSource = self
    protocolListTableView.register(UINib(nibName: "ProtocolCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
    dragBOx.layer.cornerRadius = dragBOx.bounds.height/2
    
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DismissKeyboard))
    protocolListTableView.addGestureRecognizer(tap)
    effectView.addGestureRecognizer(tap)
    view.addGestureRecognizer(tap)
  }
  
  func reloadData() {
    guard let proList = protocolList else { return }
    
    UIView.transition(with: protocolListTableView, duration: 0.15, options: .transitionCrossDissolve, animations: {
      self.protocolListTableView.reloadData()
      if proList.count > 0  {
        let indexPath = IndexPath(row: proList.count - 1, section: 0)
        self.protocolListTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
      }
    }, completion: nil)
  }
  
  @IBAction func closeButtonAction(_ sender: Any) {
    dismiss(animated: true)
  }
  
  @IBAction func addActionButton(_ sender: Any) {
    DismissKeyboard()
    guard let inputText = protocolTextField.text, !inputText.isEmpty else {
      protocolTextField.text = ""
      return
    }
    guard let filteredObjects = protocolList?.filter({ $0.name == inputText }), filteredObjects.isEmpty else {
      protocolTextField.text = ""
      return
    }
    
    let protocal = JSBridgeModel(name: inputText, value: true)
    protocolList?.append(protocal)
    reloadData()
    protocolTextField.text = ""
  }
  
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return protocolList?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = protocolListTableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ProtocolCell
    cell?.setupCell(data: protocolList![indexPath.row])
    
    
    cell?.deleteAction = {
      self.protocolList?.remove(at: indexPath.row)
      self.reloadData()
    }
    
    cell?.switchAction = { isOn in
      self.protocolList?[indexPath.row].value = isOn
    }
    
    return cell ?? UITableViewCell()
  }
  
  @objc func DismissKeyboard(){
  view.endEditing(true)
  }
  
}
