//
//  JSBridgeModel.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import Foundation

public struct JSBridgeModel: Codable {
  
  var name: String?
  var value: Bool?
  
  init(name: String, value: Bool) {
    self.name = name
    self.value = value
  }
}
