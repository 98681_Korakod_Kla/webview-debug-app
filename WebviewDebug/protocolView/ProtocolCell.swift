//
//  ProtocalCell.swift
//  WebviewDebug
//
//  Created by Korakod Saraboon on 29/4/2567 BE.
//

import UIKit

class ProtocolCell: UITableViewCell {
  
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var SButton: UISwitch!
  @IBOutlet weak var bgView: UIView!
  
  var deleteAction: (() -> Void)?
  var switchAction: ((Bool) -> Void)?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    bgView.layer.cornerRadius = 5
  }
  
  func setupCell(data: JSBridgeModel) {
    self.name.text = data.name
    SButton.isOn = data.value ?? false
  }
  
  @IBAction func trashButtonAction(_ sender: Any) {
    deleteAction?()
  }
  
  @IBAction func switchButtonAction(_ sender: UISwitch) {
    switchAction?(sender.isOn)
  }
  
  override func prepareForReuse() {
    deleteAction = nil
    name.text = ""
    SButton.isOn = false
  }
  
}
